﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using PlatformerTest.Components;
using PlatformerTest.Scenes;
using TopDownTest.Scenes;

namespace TopDownTest
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Core
    {

        public static GameStats gameStats = new GameStats();

        public Game1() : base(width: 1200, height: 800, isFullScreen: false, enableEntitySystems: false, "PlatformerTest")
        {
            IsFixedTimeStep = true;
        }

        protected override void Initialize()
        {
            base.Initialize();
            Window.AllowUserResizing = true;

            defaultSamplerState = SamplerState.PointClamp;

            Scene scene1 = new MenuScene();

            scene = scene1;
        }

    }
}
