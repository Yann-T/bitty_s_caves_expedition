﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using Nez.Tiled;
using System.Collections.Generic;

namespace TopDownTest
{
    class Player : Component, IUpdatable
    {

        public float _moveSpeed = 150f;
        public float gravity = 1800;
        public float jumpHeight = 16 * 6;

        enum Animations
        {
            Jumping,
            Falling,
            Walk,
            Idle
        }

        private float effectiveMoveSpeed;
        private VirtualButton _jumpInput;
        private VirtualButton _sprintInput;
        private VirtualIntegerAxis _xAxis;

        Sprite<Animations> _animation;
        TiledMapMover _mover;
        BoxCollider _boxCollider;
        TiledMapMover.CollisionState _collisionState = new TiledMapMover.CollisionState();
        Vector2 velocity;

        #region Methods

        public override void onAddedToEntity()
        {
            var texture = entity.scene.content.Load<Texture2D>("sprites/Blip-sheet");
            var subtextures = Subtexture.subtexturesFromAtlas(texture, 16, 16);
            _boxCollider = entity.getComponent<BoxCollider>();
            _mover = entity.getComponent<TiledMapMover>();

            _animation = entity.addComponent(new Sprite<Animations>(subtextures[0]));

            _animation.addAnimation(Animations.Walk, new SpriteAnimation(new List<Subtexture>()
            {
                subtextures[1],
                subtextures[2],
                subtextures[3]
            }));

            _animation.addAnimation(Animations.Idle, new SpriteAnimation(subtextures[0]));

            _animation.addAnimation(Animations.Falling, new SpriteAnimation(subtextures[8]));

            _animation.addAnimation(Animations.Jumping, new SpriteAnimation(new List<Subtexture>()
            {
                subtextures[4],
                subtextures[5],
                subtextures[6],
                subtextures[7]
            }));

            setInput();
        }

        public override void onRemovedFromEntity()
        {
            _xAxis.deregister();
            _jumpInput.deregister();
        }

        private void setInput()
        {
            // Mieux pour gérer l'analogique
            _xAxis = new VirtualIntegerAxis();
            _xAxis.nodes.Add(new Nez.VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehavior.TakeNewer, Keys.Left, Keys.Right));

            _jumpInput = new VirtualButton();
            _jumpInput.nodes.Add(new VirtualButton.KeyboardKey(Keys.Up));

            _sprintInput = new VirtualButton();
            _sprintInput.nodes.Add(new VirtualButton.KeyboardKey(Keys.LeftShift));

        }

        public void update()
        {
            // handle movement and animations
            var moveDir = new Vector2(_xAxis.value, 0);
            var animation = Animations.Idle;
            effectiveMoveSpeed = (_sprintInput.isDown && _collisionState.below) ? ((effectiveMoveSpeed < 2f * _moveSpeed) ? 1.05f * effectiveMoveSpeed : effectiveMoveSpeed) : ((effectiveMoveSpeed > _moveSpeed) ? effectiveMoveSpeed * 0.98f : _moveSpeed);

            if (moveDir.X < 0)
            {
                if (_collisionState.below)
                    animation = Animations.Walk;
                _animation.flipX = true;
                if (velocity.X > 0)
                {
                    effectiveMoveSpeed = _moveSpeed;
                }
                velocity.X = -effectiveMoveSpeed;

            }
            else if (moveDir.X > 0)
            {
                if (_collisionState.below)
                    animation = Animations.Walk;
                _animation.flipX = false;
                if (velocity.X < 0)
                {
                    effectiveMoveSpeed = _moveSpeed;
                }
                velocity.X = effectiveMoveSpeed;
            }
            else
            {
                velocity.X = 0;
                if (_collisionState.below)
                    animation = Animations.Idle;
            }

            if (_collisionState.below && _jumpInput.isPressed)
            {
                animation = Animations.Jumping;
                velocity.Y = -Mathf.sqrt(2f * jumpHeight * gravity);
            }

            if (!_collisionState.below && velocity.Y > 0)
                animation = Animations.Falling;

            // apply gravity
            velocity.Y += gravity * Time.deltaTime;

            // move
            _mover.move(velocity * Time.deltaTime, _boxCollider, _collisionState);

            if (_collisionState.below)
                velocity.Y = 0;

            if (!_animation.isAnimationPlaying(animation))
                _animation.play(animation);
        }
    }
    #endregion

}
