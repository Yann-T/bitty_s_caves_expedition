﻿using Microsoft.Xna.Framework.Audio;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopDownTest.Components;

namespace PlatformerTest.Components
{
    class Healh : Component, IUpdatable
    {
        private int maxHealth;
        private int currentHealth;
        private Action onZero;
        private bool isDead;
        private float cooldownTime = 1f;
        private float elapsedTime;
        private SoundEffect dmgFx;

        public Healh(int maxHealth, Action onZero) : base()
        {
            this.maxHealth = maxHealth;
            currentHealth = maxHealth;
            this.onZero = onZero;
            isDead = currentHealth <= 0;
        }

        public override void onAddedToEntity()
        {
            dmgFx = entity.scene.content.Load<SoundEffect>("sounds/death");
        }

        public void damage(int n)
        {
            if (elapsedTime >= cooldownTime)
            {
                this.currentHealth -= n;
                dmgFx.Play();
                if (currentHealth <= 0 && !isDead)
                {
                    isDead = true;
                    onZero();
                }
                else
                {
                    var comp = entity.getComponent<EntityState>();
                    if(comp != null)
                    {
                        comp.AnimState = EntityState.AnimationState.Hurt;
                    }
                    elapsedTime = 0f;
                }
            }
        }

        public void heal(int n)
        {
            this.currentHealth += n;

            if(currentHealth > maxHealth)
            {
                currentHealth = maxHealth;
            }

            if(isDead && currentHealth > 0)
            {
                isDead = false;
            }
        }

        public void update()
        {
            if(elapsedTime < cooldownTime)
            {
                elapsedTime += Time.deltaTime;
            }
        }
    }
}
