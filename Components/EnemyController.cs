﻿using Microsoft.Xna.Framework;
using Nez;
using Nez.Tiled;
using System;

namespace TopDownTest.Components
{
    class EnemyController : Component, IUpdatable
    {
        #region genericProperties
        private Vector2 velocity;
        private SubpixelVector2 subpixelVector = new SubpixelVector2();
        private TiledMapMover.CollisionState collisionState = new TiledMapMover.CollisionState();
        private TiledMapMover mover;
        private BoxCollider boxCollider;
        private EntityState entityState;
        private float effectiveMoveSpeed;
        private float moveSpeed = 20;
        private float gravity = 1000;
        private float waitingTime = 2;
        private float waitedTime = 2;

        #endregion

        public EnemyController() : base()
        { }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            velocity = new Vector2(moveSpeed * Time.deltaTime, 0);

            boxCollider = entity.getComponent<BoxCollider>();
            mover = entity.getComponent<TiledMapMover>();
            entityState = entity.getComponent<EntityState>();

            changeAnimationState(EntityState.AnimationState.Walking);
            effectiveMoveSpeed = -moveSpeed;
            entityState.FlipX = true;
        }

        public void update()
        {

            if (collisionState.below && Math.Abs(velocity.X) > 0.1 && !(collisionState.left || collisionState.right))
            {
                changeAnimationState(EntityState.AnimationState.Walking);
            }
            else
            {
                changeAnimationState(EntityState.AnimationState.Idle);
            }
            if (collisionState.left && velocity.X < 0 && waitedTime >= waitingTime)
            {
                waitedTime = 0;
                changeAnimationState(EntityState.AnimationState.Idle);
            }

            if (collisionState.right && velocity.X > 0 && waitedTime >= waitingTime)
            {
                waitedTime = 0;
                changeAnimationState(EntityState.AnimationState.Idle);
            }

            if (waitedTime < waitingTime)
            {
                waitedTime += Time.deltaTime;
            }

            if (!collisionState.below && velocity.Y > 0)
            {
                changeAnimationState(EntityState.AnimationState.Falling);
            }

            if ((collisionState.left || collisionState.right) && waitedTime < waitingTime)
            {
                effectiveMoveSpeed = -effectiveMoveSpeed;

                changeAnimationState(EntityState.AnimationState.Idle);
                entityState.FlipX = !entityState.FlipX;
            }

            if (collisionState.below && waitedTime >= waitingTime)
            {
                velocity.X = effectiveMoveSpeed;
            }

            velocity.Y += gravity * Time.deltaTime;

            velocity.X *= Time.deltaTime;
            Vector2 effectiveVelocity = new Vector2(velocity.X, velocity.Y * Time.deltaTime);
            subpixelVector.update(ref effectiveVelocity);
            mover.move(effectiveVelocity, boxCollider, collisionState);

            if (collisionState.below)
            {
                velocity.Y = 0;
            }
        }

        private void changeAnimationState(EntityState.AnimationState newState)
        {
            entityState.AnimState = newState;
        }

    }
}
