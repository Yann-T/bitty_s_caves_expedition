﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using Nez;
using Nez.Tiled;
using System;

namespace TopDownTest.Components
{
    partial class PlayerController : Component, IUpdatable
    {
        #region genericProperties
        private Vector2 velocity;
        private TiledMapMover.CollisionState collisionState = new TiledMapMover.CollisionState();
        private TiledMapMover mover;
        private BoxCollider boxCollider;
        private EntityState entityState;
        private float moveSpeed = 210;
        private float gravity = 1000;
        private float jumpHeight = 12 * 3.7f;
        private bool lastRightCollision = false;
        private bool lastLeftCollision = false;
        private SoundEffect jumpFx;

        #endregion

        #region inputs

        private VirtualIntegerAxis xAxisInput;
        private VirtualButton jumpInput;
        private bool jumpWasDownPreviousFrame;

        #endregion

        public override void onAddedToEntity()
        {
            jumpFx = entity.scene.content.Load<SoundEffect>("sounds/jump");
            getComponents();
            setInput();
        }

        #region InitMethods
        private void setInput()
        {
            xAxisInput = new VirtualIntegerAxis();
            xAxisInput.nodes.Add(new Nez.VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehavior.TakeNewer, Keys.Left, Keys.Right));

            jumpInput = new VirtualButton();
            jumpInput.nodes.Add(new VirtualButton.KeyboardKey(Keys.Z));
            jumpInput.nodes.Add(new VirtualButton.KeyboardKey(Keys.W));
            jumpInput.nodes.Add(new VirtualButton.KeyboardKey(Keys.Space));
        }

        private void getComponents()
        {
            boxCollider = entity.getComponent<BoxCollider>();
            mover = entity.getComponent<TiledMapMover>();
            entityState = entity.getComponent<EntityState>();
        }

        #endregion

        public override void onRemovedFromEntity()
        {
            xAxisInput.deregister();
            jumpInput.deregister();
        }

        public void update()
        {
            if (collisionState.below)
            {
                changeAnimationState(EntityState.AnimationState.Idle);
            }
            Vector2 moveDir = initializeMoveDir();
            xAxisInputHandler(moveDir);
            jumpInputHandler();
            applyGravityAndMove();
        }

        #region inputHandlers
        private void xAxisInputHandler(Vector2 moveDir)
        {
            if(Math.Abs(velocity.X) <= 0.5f || (velocity.X > 0 && collisionState.right) || (velocity.X < 0 && collisionState.left))
            {
                velocity.X = 0;
            }
            else
            {
                if (collisionState.below)
                {
                    velocity.X *= 0.54f;
                } else
                {
                    if (xAxisInput.value == 0)
                    {
                        velocity.X *= 0.85f;
                    }
                    else
                    {
                        velocity.X *= 0.8f;
                    }
                }
                
            }

            if (moveDir.X < 0)
            {
                entityState.FlipX = true;
                if (collisionState.below)
                {
                    changeAnimationState(EntityState.AnimationState.Walking);
                } else
                {
                    changeAnimationState(EntityState.AnimationState.Falling);
                }
                if (collisionState.below) { 
                    velocity.X -= 0.2f * moveSpeed;
                } else
                {
                    velocity.X -= 0.1f * moveSpeed;
                }
            }

            if (moveDir.X > 0)
            {
                entityState.FlipX = false;
                if (collisionState.below)
                {
                    changeAnimationState(EntityState.AnimationState.Walking);
                } else
                {
                    changeAnimationState(EntityState.AnimationState.Falling);
                }
                if (collisionState.below)
                {
                    velocity.X += 0.2f * moveSpeed;
                }
                else
                {
                    velocity.X += 0.1f * moveSpeed;
                }
            }
        }
        private void jumpInputHandler()
        {
            if (collisionState.below && jumpInput.isPressed)
            {
                jumpFx.Play();
                changeAnimationState(EntityState.AnimationState.Jumping);
                velocity.Y = -Mathf.sqrt(2f * jumpHeight * gravity);
                //velocity.X *= 1.5f;
                jumpWasDownPreviousFrame = true;
            }
            if(((collisionState.left || collisionState.right || lastRightCollision || lastLeftCollision) && velocity.Y >= 0) && jumpInput.isPressed && (xAxisInput.value != 0))
            {
                jumpFx.Play();
                changeAnimationState(EntityState.AnimationState.Jumping);
                velocity.Y = -Mathf.sqrt(2f * jumpHeight * gravity);
                if(collisionState.right || lastRightCollision)
                {
                    velocity.X -= 1.2f * moveSpeed;
                } else
                {
                    velocity.X += 1.2f * moveSpeed;
                }

                jumpWasDownPreviousFrame = true;
            }
            if (jumpInput.isReleased && jumpWasDownPreviousFrame)
            {
                velocity.Y *=0.5f;
                jumpWasDownPreviousFrame = false;
            }

            if (collisionState.above)
            {
                
                if (velocity.Y < 0)
                {
                    velocity.Y = gravity * Time.deltaTime;
                }
            }
        }

        #endregion

        #region other
        private Vector2 initializeMoveDir()
        {
            return new Vector2(xAxisInput.value, 0);
        }
        private void applyGravityAndMove()
        {
            if (!collisionState.below &&  collisionState.left && velocity.Y > 0 && (xAxisInput.value != 0))
            {
                velocity.Y *= 0.7f;
                entityState.FlipX = true;
                changeAnimationState(EntityState.AnimationState.WallSliding);
            }
            if (!collisionState.below && collisionState.right && velocity.Y > 0 && (xAxisInput.value != 0))
            {
                velocity.Y *= 0.7f;
                entityState.FlipX = false;
                changeAnimationState(EntityState.AnimationState.WallSliding);
            }
            if (!collisionState.below && velocity.Y >= 0 && (!(collisionState.left || collisionState.right) || (xAxisInput.value == 0)))
            {
                changeAnimationState(EntityState.AnimationState.Falling);
            }

            velocity.Y += gravity * Time.deltaTime;

            // BOF TIER/20
            //subpixelVector2.update(ref velocity);
            //smoothMovement();
            mover.move(velocity * Time.deltaTime, boxCollider, collisionState);
            if (collisionState.below)
            {
                velocity.Y = 0;
            }

            lastLeftCollision = collisionState.left;
            lastRightCollision = collisionState.right;
        }

        private void changeAnimationState(EntityState.AnimationState newState)
        {
            entityState.AnimState = newState;
        }

        private void smoothMovement()
        {
            var effectiveVelocity = velocity * Time.deltaTime;
            if (!(collisionState.right || collisionState.left || collisionState.above))
            {
                while (effectiveVelocity.Y > 0.5)
                {
                    mover.move(new Vector2(0, 1), boxCollider, collisionState);
                    effectiveVelocity.Y -= 1;
                }
                while (effectiveVelocity.Y < -0.5)
                {
                    mover.move(new Vector2(0, -1), boxCollider, collisionState);
                    effectiveVelocity.Y += 1;
                }
                while (effectiveVelocity.X > 0.5)
                {
                    mover.move(new Vector2(1, 0), boxCollider, collisionState);
                    effectiveVelocity.X -= 1;
                }
                while (effectiveVelocity.X < -0.5 && !collisionState.left)
                {
                    mover.move(new Vector2(-1, 0), boxCollider, collisionState);
                    effectiveVelocity.X += 1;

                }
            }
            else
            {
                mover.move(velocity * Time.deltaTime, boxCollider, collisionState);
            }
        }

        #endregion
    }

}
