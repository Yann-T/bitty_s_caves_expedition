﻿using Microsoft.Xna.Framework;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopDownTest.Components;
using TopDownTest.Scenes;

namespace PlatformerTest.Components
{
    class Fireball : Component, IUpdatable
    {
        private Vector2 velocity;
        private Collider collider;
        private SubpixelVector2 subpixelvector;
        private Mover mover;
        float speed = 150f;
        public Fireball(Vector2 direction) : base()
        {
            velocity = direction * speed;
        }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            collider = entity.getComponent<Collider>();
            mover = entity.getComponent<Mover>();
            collider.isTrigger = true;
        }

        public void update()
        {
            if (collider.collidesWithAny(out var res))
            {
                entity.destroy();
            }
            else
            {
                var movement = new Vector2(velocity.X * Time.deltaTime, velocity.Y * Time.deltaTime);
                mover.calculateMovement(ref movement, out var colRes);
                subpixelvector.update(ref movement);
                mover.applyMovement(movement);
            }
        }
    }
}
