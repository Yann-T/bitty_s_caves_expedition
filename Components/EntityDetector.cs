﻿using Nez;
using System;

namespace TopDownTest.Components
{
    class EntityDetector : Component, IUpdatable
    {
        Entity toDetect;
        Collider selfCollider;
        Collider entityCollider;
        Action action;

        public EntityDetector(Entity entity, Action action) : base()
        {
            toDetect = entity;
            this.action = action;
        }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            selfCollider = entity.getComponent<Collider>();
            entityCollider = toDetect.getComponent<Collider>();
        }

        public void update()
        {
            if (selfCollider.collidesWith(entityCollider, out var result))
            {
                action();
            }
        }
    }
}
