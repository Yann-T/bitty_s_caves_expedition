﻿using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using System.Collections.Generic;

namespace TopDownTest.Components
{
    class AnimatedSprite : Component, IUpdatable
    {
        #region properties
        private Dictionary<EntityState.AnimationState, List<int>> animationsFrames;
        private string path;
        private int width;
        private int height;
        private EntityState entityState;
        Sprite<EntityState.AnimationState> animation;

        public int FPS { get; set; } = 12;

        #endregion
        public AnimatedSprite(string spritesheetPath, int frameWidth, int frameHeight)
        {
            path = spritesheetPath;
            width = frameWidth;
            height = frameHeight;
            animationsFrames = new Dictionary<EntityState.AnimationState, List<int>>();
        }

        public void addAnimation(EntityState.AnimationState state, List<int> frames)
        {
            animationsFrames.Add(state, frames);
        }

        public override void onAddedToEntity()
        {
            var texture = entity.scene.content.Load<Texture2D>("sprites/" + path);
            var subtextures = Subtexture.subtexturesFromAtlas(texture, width, height);
            animation = entity.addComponent(new Sprite<EntityState.AnimationState>(subtextures[0]));
            entityState = entity.getComponent<EntityState>();

            initializeAllAnimations(subtextures);
        }

        private void initializeAllAnimations(List<Subtexture> subtextures)
        {
            foreach (var item in animationsFrames)
            {
                initializeAnimation(subtextures, item);
            }
        }

        private void initializeAnimation(List<Subtexture> subtextures, KeyValuePair<EntityState.AnimationState, List<int>> item)
        {
            SpriteAnimation anim = new SpriteAnimation();
            foreach (var frame in item.Value)
            {
                anim.addFrame(subtextures[frame]);
            }
            anim.setFps(FPS);
            if (item.Key == EntityState.AnimationState.Jumping)
            {
                anim.loop = false;
                anim.setFps(20);
            }
            if (item.Key == EntityState.AnimationState.WallSliding)
            {
                anim.loop = false;
            }

            animation.addAnimation(item.Key, anim);
        }

        public void update()
        {
            animation.flipX = entityState.FlipX;
            animation.flipY = entityState.FlipY;

            if (!animation.isAnimationPlaying(entityState.AnimState))
            {
                if (animationsFrames.ContainsKey(entityState.AnimState))
                {
                    animation.play(entityState.AnimState);

                }
                else
                {
                    animation.play(EntityState.AnimationState.Idle);
                }
            }
        }
    }
}
