﻿using Microsoft.Xna.Framework.Audio;
using Nez;
using System;

namespace TopDownTest.Components
{
    class Collectible : Component, IUpdatable
    {
        private SoundEffect pickupSound;
        private Entity collector;
        private CircleCollider self;
        private Collider collectorCollider;

        public Collectible(Entity collector) : base()
        {
            this.collector = collector;
        }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();

            pickupSound = entity.scene.content.Load<SoundEffect>("sounds/coin");
            self = entity.getComponent<CircleCollider>();
            collectorCollider = collector.getComponent<Collider>();
        }
        public void update()
        {
            if (collectorCollider.collidesWith(self, out var result))
            {
                pickupSound.Play();
                var state = collector.getComponent<EntityState>();
                if(state != null)
                {
                    Game1.gameStats.CoinsPicked++;
                }
                entity.destroy();
            }
        }
    }
}
