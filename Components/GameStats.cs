﻿using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformerTest.Components
{
    public class GameStats
    {
        private int coinsPicked;
        private int roomCleared;
        private static int numberOfMaps = 11;
        private static int mapToPlayPerRun = numberOfMaps;
        private int mapPlayed;
        private List<int> mapsInRun;

        public int CoinsPicked { get => coinsPicked; set => coinsPicked = value; }
        public int RoomCleared { get => roomCleared; set => roomCleared = value; }

        public GameStats()
        {
            coinsPicked = 0;
            roomCleared = 0;
            mapPlayed = 0;
            mapsInRun = new List<int>();
            generateMapsToPlayInOrder();
        }

        private void generateMapsToPlayInOrder()
        {
            for(int i = 1; i <= numberOfMaps; i++)
            {
                mapsInRun.Add(i);
            }
        }

        private void generateMapsToPlay()
        {
            List<int> availableMaps = resetAvailableMaps();

            for (int i = 0; i < mapToPlayPerRun; i++)
            {
                int j;
                do
                {
                    j = Nez.Random.nextInt(numberOfMaps) + 1;
                } while (!availableMaps.Contains(j));

                availableMaps.Remove(j);
                mapsInRun.Add(j);

                if(availableMaps.Count == 0)
                {
                    availableMaps = resetAvailableMaps();
                }

            }
        }

        private List<int> resetAvailableMaps()
        {
            var availableMaps = new List<int>();

            for(int i = 1; i < numberOfMaps + 1; i++)
            {
                availableMaps.Add(i);
            }
            return availableMaps;
        }

        public int getNextMap()
        {
            if (mapPlayed >= mapToPlayPerRun)
            {
                return -1;
            }
            return mapsInRun.ElementAt(mapPlayed++);
        }
        public void resetGame()
        {
            mapPlayed = 0;
            coinsPicked = 0;
            roomCleared = 0;
            mapsInRun = new List<int>();
            generateMapsToPlayInOrder();
        }
    }
}
