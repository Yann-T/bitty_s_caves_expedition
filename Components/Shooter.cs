﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopDownTest.Components;
using TopDownTest.Scenes;

namespace PlatformerTest.Components
{
    class Shooter : Component, IUpdatable
    {
        public float delay = 2f;
        public float offset = 0f;
        private float elapsedTime = 0f;
        private int max_fireball = 10;
        private int next_fireball;
        private Entity[] fireballs;
        private Vector2 fireball_velocity;
        private string mapPrefix;
        private Entity player;
        private SoundEffect fireFx;
        //private Entity fireball;
        public Shooter(Direction dir, Entity player, string mapPrefix)
        {
            this.fireballs = new Entity[max_fireball];
            next_fireball = 0;
            this.player = player;
            this.mapPrefix = mapPrefix;
            switch (dir)
            {
                case Direction.Left: fireball_velocity = new Vector2(-1, 0); break;
                case Direction.Right: fireball_velocity = new Vector2(1, 0); break;
                case Direction.Up: fireball_velocity = new Vector2(0, -1); break;
                case Direction.Down: fireball_velocity = new Vector2(0, 1); break;
                default: fireball_velocity = new Vector2(0, 0); break;
            }
        }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            fireFx = entity.scene.content.Load<SoundEffect>("sounds/shoot");
        }

        public void update()
        {
            elapsedTime += Time.deltaTime;

            if(elapsedTime >= delay + offset)
            {
                fireFx.Play();
                offset = 0f;
                elapsedTime = 0;
                fireballs[next_fireball] = entity.scene.createEntity(entity.name + "_fireball_" + next_fireball, new Vector2(entity.position.X + 12 * fireball_velocity.X, entity.position.Y - 3 + 12 * fireball_velocity.Y));

                fireballs[next_fireball].addComponent<Fireball>(new Fireball(fireball_velocity));
                var state = fireballs[next_fireball].addComponent<EntityState>(new EntityState());
                fireballs[next_fireball].addComponent<CircleCollider>(new CircleCollider(2));
                fireballs[next_fireball].addComponent<Mover>(new Mover());

                if(fireball_velocity.X > 0)
                {
                    state.FlipX = true;
                }
                if(fireball_velocity.Y < 0)
                {
                    fireballs[next_fireball].transform.setRotation(90f * (float)Math.PI / 180f);
                }
                if (fireball_velocity.Y > 0)
                {
                    fireballs[next_fireball].transform.setRotation(-90f * (float)Math.PI / 180f);
                }

                var sprite = fireballs[next_fireball].addComponent<AnimatedSprite>(new AnimatedSprite("fireball2", 12, 12));
                sprite.addAnimation(EntityState.AnimationState.Idle, new List<int>() { 0, 1, 2, 3 });
                fireballs[next_fireball].addComponent<DealDamage>(new DealDamage(1));

                next_fireball = (next_fireball + 1) % max_fireball;
            }
        }



    }
}
