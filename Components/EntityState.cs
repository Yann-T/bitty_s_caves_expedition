﻿using Nez;

namespace TopDownTest.Components
{
    public class EntityState : Component
    {
        private AnimationState animState;
        private bool flipX;
        private bool flipY;
        public EntityState()
        {
            animState = AnimationState.Idle;
        }

        public AnimationState AnimState { get => animState; set => animState = value; }
        public bool FlipX { get => flipX; set => flipX = value; }
        public bool FlipY { get => flipY; set => flipY = value; }

        public enum AnimationState
        {
            Walking,
            Running,
            Jumping,
            Falling,
            Idle,
            Dead,
            WallSliding,
            Hurt
        }
    }
}
