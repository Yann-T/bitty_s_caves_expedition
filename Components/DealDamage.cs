﻿using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformerTest.Components
{
    class DealDamage : Component, IUpdatable
    {
        private int damageToDeal;
        private Collider self;

        public DealDamage(int howMuch)
        {
            damageToDeal = howMuch;
        }

        public override void onAddedToEntity()
        {
            self = entity.getComponent<Collider>();
        }

        public void update()
        {
            if (self.collidesWithAny(out var res))
            {
                var hp = res.collider.getComponent<Healh>();
                if (hp != null)
                {
                    hp.damage(damageToDeal);
                }
            }
        }
    }
}
