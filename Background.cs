﻿using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.Sprites;

namespace TopDownTest
{
    class Background : ScrollingSprite
    {
        public Camera Camera { get; set; }

        public Background(Texture2D tex, Camera camera) : base(tex)
        {
            Camera = camera;
        }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
        }

        public override void render(Graphics graphics, Camera camera)
        {
            this.entity.position = camera.position;
            base.render(graphics, camera);
        }
    }
}
