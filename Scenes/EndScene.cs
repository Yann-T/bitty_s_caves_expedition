﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.UI;
using TopDownTest;
using TopDownTest.Scenes;

namespace PlatformerTest.Scenes
{
    class EndScene : BaseScene
    {
        private UICanvas canvas;
        private Table table;
        List<Button> sceneButtons = new List<Button>();
        public EndScene() : base(Color.Black)
        {
            canvas = createEntity("ui").addComponent(new UICanvas());
            //canvas.isFullScreen = true;
            setupUI();
        }

        public override void initialize()
        {
            setDesignResolution(12 * 16, 12 * 16, SceneResolutionPolicy.ShowAllPixelPerfect);
        }

        public override void onStart()
        {
            base.onStart();
        }

        private void setupUI()
        {

            table = canvas.stage.addElement(new Table());
            table.setFillParent(true).center();

            table.add(new Label("You picked " + Game1.gameStats.CoinsPicked + " coins \nAnd cleared " + Game1.gameStats.RoomCleared + " rooms !")).setFillX().setMinHeight(30);

            table.row().setPadTop(10);
            var playButtonStyle = new TextButtonStyle(new PrimitiveDrawable(100f, 10f, Color.White), new PrimitiveDrawable(Color.White), new PrimitiveDrawable(Color.Black))
            {
                downFontColor = Color.Black,
                fontColor = Color.Black,
                overFontColor = Color.White
            };
            var btn = table.add(new TextButton("Restart", playButtonStyle)).setFillX().setMinHeight(30).getElement<Button>();
            btn.onClicked += (Button button) => {
                destroyAllEntities();
                Game1.gameStats.resetGame();
                /*FadeTransition trans = new FadeTransition(() => Activator.CreateInstance(typeof(PlateformerScene), new object[] { Color.Black, "test/map", Game1.gameStats.getNextMap() }) as Scene);
                trans.fadeInDuration = 0.5f;
                trans.fadeOutDuration = 2f;
                Core.startSceneTransition(trans);*/
                Core.scene = new PlateformerScene(Color.Black, "test/map", Game1.gameStats.getNextMap());
            };
        }
    }
}
