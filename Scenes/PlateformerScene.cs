﻿using Microsoft.Xna.Framework;
using Nez;
using Nez.Tiled;
using PlatformerTest.Components;
using PlatformerTest.Scenes;
using System;
using System.Collections.Generic;
using TopDownTest.Components;

namespace TopDownTest.Scenes
{
    class PlateformerScene : BaseScene
    {
        private static float shootersDelay = 2f;
        string mapPrefixe;
        int mapNumber;

        enum RenderLayer
        {
            FG = -1,
            TileMap = 10,
            BG = 20
        }

        public PlateformerScene(Color? clearColor, string mapPrefixe, int mapNumber) : base(clearColor)
        {
            this.mapPrefixe = mapPrefixe;
            this.mapNumber = mapNumber;

        }

        public override void initialize()
        {
            setDesignResolution(12 * 32, 6 * 36, SceneResolutionPolicy.ShowAllPixelPerfect);
            //Screen.setSize(192 * 5, 192 * 5);
        }

        public override void onStart()
        {
            string mapPath = "maps/" + mapPrefixe + mapNumber;

            TiledMap tiledMap = content.Load<TiledMap>(mapPath);
            var objectLayer = tiledMap.getObjectGroup("obj");
            var spawnObject = objectLayer.objectWithName("entry");
            var exitObject = objectLayer.objectWithName("exit");
            var coinObjects = objectLayer.objectsWithName("coin");
            var enemyObjects = objectLayer.objectsWithName("enemy");
            var shootersLeft = objectLayer.objectsWithName("shooter_left");
            var shootersRight = objectLayer.objectsWithName("shooter_right");
            var shootersTop = objectLayer.objectsWithName("shooter_top");
            var shootersBot = objectLayer.objectsWithName("shooter_bot");

            TiledMapComponent tiledMapComponent;
            Entity tiledEntityBG;
            InitTiledMapEntityBackground(tiledMap, out tiledMapComponent, out tiledEntityBG);
            InitTiledMapEntityForeground(tiledMap);
            InitCameraBounds(tiledMap, tiledEntityBG);
            Entity playerEntity = InitPlayerEntity(spawnObject, tiledMapComponent);
            InitExitEntity(exitObject, playerEntity);
            InitCoinsEntities(tiledMap, coinObjects, playerEntity);
            InitEnemyEntities(tiledMap, enemyObjects, playerEntity, tiledMapComponent);
            InitShooterEntities(tiledMap, shootersLeft, playerEntity, tiledMapComponent, Direction.Left, mapPrefixe);
            InitShooterEntities(tiledMap, shootersBot, playerEntity, tiledMapComponent, Direction.Down, mapPrefixe);
            InitShooterEntities(tiledMap, shootersRight, playerEntity, tiledMapComponent, Direction.Right, mapPrefixe);
            InitShooterEntities(tiledMap, shootersTop, playerEntity, tiledMapComponent, Direction.Up, mapPrefixe);
            InitFollowCamera(playerEntity);
            InitPostProcessors();

            base.onStart();
        }

        #region other
        private Entity InitPlayerEntity(TiledObject spawnObject, TiledMapComponent tiledMapComponent)
        {
            Entity playerEntity = createEntity("player", new Vector2(spawnObject.x, spawnObject.y));
            playerEntity.addComponent(new EntityState());
            playerEntity.addComponent(new PlayerController());
            AnimatedSprite sprite = new AnimatedSprite("Bitty", 12, 12);
            sprite.addAnimation(EntityState.AnimationState.Idle, new List<int>() { 0, 1, 2, 3, 4, 5 });
            sprite.addAnimation(EntityState.AnimationState.Walking, new List<int>() { 7, 8, 9, 10 });
            sprite.addAnimation(EntityState.AnimationState.Jumping, new List<int>() { 11, 12, 13, 14 });
            sprite.addAnimation(EntityState.AnimationState.Falling, new List<int>() { 2 });
            sprite.addAnimation(EntityState.AnimationState.WallSliding, new List<int>() { 15, 16 });
            playerEntity.addComponent(sprite);
            playerEntity.addComponent(new BoxCollider(-6, -6, 12, 12));
            playerEntity.addComponent(new TiledMapMover(tiledMapComponent.collisionLayer));
            playerEntity.addComponent(new Healh(1, () =>
            {
                destroyAllEntities();
                FadeTransition trans = new FadeTransition(() => Activator.CreateInstance(typeof(PlateformerScene), new object[] { Color.Black, mapPrefixe, mapNumber }) as Scene);
                trans.fadeInDuration = 0.3f;
                trans.fadeOutDuration = 0.8f;
                Core.startSceneTransition(trans);
            }));
            return playerEntity;
        }

        private void InitTiledMapEntityForeground(TiledMap tiledMap)
        {
            var tiledEntity = createEntity("tiledMapEntityfg");
            var tiledMapComponent = tiledEntity.addComponent(new TiledMapComponent(tiledMap));
            tiledMapComponent.setLayersToRender(new string[] { "fg1", "fg2" });
        }

        private void InitTiledMapEntityBackground(TiledMap tiledMap, out TiledMapComponent tiledMapComponent, out Entity tiledEntity)
        {
            tiledEntity = createEntity("tiledMapEntity");
            tiledMapComponent = tiledEntity.addComponent(new TiledMapComponent(tiledMap, "solid", true));
            tiledMapComponent.setLayersToRender(new string[] { "solid", "bg1", "bg2" });
            tiledMapComponent.renderLayer = (int)RenderLayer.TileMap;
        }

        private void InitPostProcessors()
        {
            /*var effect = new BloomPostProcessor(0);
            this.addPostProcessor(effect);
            effect.samplerState = SamplerState.AnisotropicWrap;
            effect.settings = new BloomSettings(1f, 0.5f, .5f, 1.2f, 1.5f, 1.2f);*/
        }

        private FollowCamera InitFollowCamera(Entity playerEntity)
        {
            var followCam = camera.entity.addComponent(new FollowCamera(playerEntity));
            followCam.camera = camera;
            followCam.mapLockEnabled = true;
            return followCam;
        }

        private void InitCoinsEntities(TiledMap tiledMap, List<TiledObject> coinObjects, Entity playerEntity)
        {
            int i = 0;
            foreach (var coin in coinObjects)
            {
                Point tile = tiledMap.worldToTilePosition(coin.position);
                Vector2 center = new Vector2((tile.X * tiledMap.tileWidth) + tiledMap.tileWidth / 2, (tile.Y * tiledMap.tileHeight) + tiledMap.tileHeight / 2);
                var coinEntity = createEntity("coin" + (i++), new Vector2(center.X, center.Y));
                coinEntity.addComponent<Collectible>(new Collectible(playerEntity));
                coinEntity.addComponent<CircleCollider>(new CircleCollider(5));
                var coinSprite = new AnimatedSprite("coin", 12, 12);
                coinSprite.addAnimation(EntityState.AnimationState.Idle, new List<int>() { 0, 1, 2, 3, 4, 5 });
                coinEntity.addComponent<AnimatedSprite>(coinSprite);
                coinEntity.addComponent<EntityState>(new EntityState());

            }
        }

        private void InitShooterEntities(TiledMap tiledMap, List<TiledObject> shooterObjects, Entity playerEntity, TiledMapComponent tiledMapComponent, Direction direction, string mapPrefix)
        {
            int i = 0;
            foreach (var shooter in shooterObjects)
            {
                Point tile = tiledMap.worldToTilePosition(shooter.position);
                Vector2 center = new Vector2((tile.X * tiledMap.tileWidth) + tiledMap.tileWidth / 2, (tile.Y * tiledMap.tileHeight) + tiledMap.tileHeight / 2);
                var shooterEntity = createEntity("shooter" + (i++), new Vector2(center.X, center.Y));
                var shooterComp = shooterEntity.addComponent<Shooter>(new Shooter(direction, playerEntity, mapPrefix));
                shooterComp.delay = shootersDelay;
                shooterComp.offset = 0;
                
            }
        }

        private void InitEnemyEntities(TiledMap tiledMap, List<TiledObject> enemyObjects, Entity playerEntity, TiledMapComponent tiledMapComponent)
        {
            int i = 0;
            foreach (var enemy in enemyObjects)
            {
                Point tile = tiledMap.worldToTilePosition(enemy.position);
                Vector2 center = new Vector2((tile.X * tiledMap.tileWidth) + tiledMap.tileWidth / 2, (tile.Y * tiledMap.tileHeight) + tiledMap.tileHeight / 2);
                var enemyEntity = createEntity("slime" + (i++), new Vector2(center.X, center.Y));
                var collider = enemyEntity.addComponent<BoxCollider>(new BoxCollider(10, 8));
                collider.setLocalOffset(new Vector2(0, 2));
                collider.isTrigger = true;
                var enemySprite = new AnimatedSprite("slime", 12, 12);
                enemySprite.addAnimation(EntityState.AnimationState.Walking, new List<int>() { 0, 1, 2, 3, 4, 5 });
                enemySprite.addAnimation(EntityState.AnimationState.Idle, new List<int>() { 6, 7, 8, 9, 10, 11 });

                enemyEntity.addComponent<AnimatedSprite>(enemySprite);
                enemyEntity.addComponent<EntityState>(new EntityState());
                enemyEntity.addComponent<TiledMapMover>(new TiledMapMover(tiledMapComponent.collisionLayer));
                enemyEntity.addComponent<EnemyController>(new EnemyController());
                enemyEntity.addComponent(new DealDamage(1));
            }
        }

        private Entity InitExitEntity(TiledObject exitObject, Entity playerEntity)
        {
            var exitEntity = createEntity("exit", new Vector2(exitObject.x, exitObject.y));
            exitEntity.addComponent(new BoxCollider(0, 0, exitObject.width, exitObject.height));
            exitEntity.addComponent(new EntityDetector(playerEntity, () =>
            {
                destroyAllEntities();
                int nextMap = Game1.gameStats.getNextMap();
                FadeTransition trans;
                if (nextMap < 0)
                {
                    trans = new FadeTransition(() => Activator.CreateInstance(typeof(MenuScene)) as Scene);
                } else
                {
                    Game1.gameStats.RoomCleared++;
                    trans = new FadeTransition(() => Activator.CreateInstance(typeof(PlateformerScene), new object[] { Color.Black, mapPrefixe, nextMap }) as Scene);
                }
                trans.fadeInDuration = 0.2f;
                trans.fadeOutDuration = 0.2f;
                Core.startSceneTransition(trans);
                
            }));
            return exitEntity;
        }

        private static void InitCameraBounds(TiledMap tiledMap, Entity tiledEntity)
        {
            var topLeft = new Vector2(tiledMap.tileWidth, tiledMap.tileWidth);
            var bottomRight = new Vector2(tiledMap.tileWidth * (tiledMap.width - 1), tiledMap.tileWidth * (tiledMap.height - 1));
            tiledEntity.addComponent(new CameraBounds(topLeft, bottomRight));
        }

        #endregion
    }
}
