﻿using Microsoft.Xna.Framework;
using Nez;

namespace TopDownTest.Scenes
{
    class BaseScene : Scene
    {
        public BaseScene(Color? clrColor) : base()
        {
            if (clrColor.HasValue)
                clearColor = clrColor.Value;
            addRenderer(new DefaultRenderer());
        }
    }
}
