﻿using Microsoft.Xna.Framework;
using Nez;
using Nez.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopDownTest;
using TopDownTest.Scenes;

namespace PlatformerTest.Scenes
{
    class MenuScene : BaseScene
    {
        private UICanvas canvas;
        private Table table;
        List<Button> sceneButtons = new List<Button>();
        public MenuScene() : base(Color.Black)
        {
            canvas = createEntity("ui").addComponent(new UICanvas());
            //canvas.isFullScreen = true;
            setupUI();
        }

        public override void initialize()
        {
            setDesignResolution(12 * 16, 12 * 16, SceneResolutionPolicy.ShowAllPixelPerfect);
        }

        public override void onStart()
        {
            base.onStart();
        }

        private void setupUI()
        {
            table = canvas.stage.addElement(new Table());
            table.setFillParent(true).center();
            var playButtonStyle = new TextButtonStyle(new PrimitiveDrawable(100f, 10f, Color.White), new PrimitiveDrawable(Color.White), new PrimitiveDrawable(Color.Black))
            {
                downFontColor = Color.Black, fontColor = Color.Black, overFontColor = Color.White
            };
            var btn = table.add(new TextButton("Play", playButtonStyle)).setFillX().setMinHeight(30).getElement<Button>();
            btn.onClicked += (Button button) => {
                destroyAllEntities();
                Game1.gameStats.resetGame();
                FadeTransition trans = new FadeTransition(() => Activator.CreateInstance(typeof(PlateformerScene), new object[] { Color.Black, "map", Game1.gameStats.getNextMap() }) as Scene);
                //FadeTransition trans = new FadeTransition(() => Activator.CreateInstance(typeof(PlateformerScene), new object[] { Color.Black, "map", 11 }) as Scene);
                trans.fadeInDuration = 0.5f;
                trans.fadeOutDuration = 2f;
                Core.startSceneTransition(trans);
            };

            table.row().setPadTop(10);

            var btn2 = table.add(new TextButton("Exit", playButtonStyle)).setFillX().setMinHeight(30).getElement<Button>();
            btn2.onClicked += (Button button) => {
                Core.exit();
            };
        }
    }
}
